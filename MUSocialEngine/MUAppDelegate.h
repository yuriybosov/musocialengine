//
//  MUAppDelegate.h
//  MUSocialEngine
//
//  Created by Yuriy Bosov on 9/11/12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DXViewController;

@interface MUAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) DXViewController *viewController;

@end
