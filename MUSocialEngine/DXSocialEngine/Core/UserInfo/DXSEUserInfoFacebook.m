//
//  DXSEUserInfo.m
//  SocialEngine
//
//  Created by Malaar on 3/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DXSEUserInfoFacebook.h"

@implementation DXSEUserInfoFacebook

@synthesize name, email, avatarURL, birthdayDate;
@synthesize sex;
@synthesize firstName, lastName;
@synthesize userLocation;

//==============================================================================
+ (id) userInfo
{
    return [[[DXSEUserInfoFacebook alloc] init] autorelease];
}

//==============================================================================
- (void) dealloc
{
    [name release];
    [email release];
    [avatarURL release];
    [birthdayDate release];
    [userLocation release];
    [firstName release];
    [lastName release];
    
    [super dealloc];
}

//==============================================================================
- (NSString*) description
{
    return [NSString stringWithFormat:@"id=%@; name=%@; email=%@; birthdate=%@; avatarURL=%@; userLocation=%@",
            self.ID, name, email, birthdayDate, avatarURL, [userLocation description]];
}

@end
