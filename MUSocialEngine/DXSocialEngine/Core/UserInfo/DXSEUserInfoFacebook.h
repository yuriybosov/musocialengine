//
//  DXSEUserInfo.h
//  SocialEngine
//
//  Created by Malaar on 3/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DXSEUserInfo.h"
#import "DXSEFacebookUserLocation.h"

@interface DXSEUserInfoFacebook : DXSEUserInfo

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *firstName;
@property (nonatomic, retain) NSString *lastName;
@property (nonatomic, retain) NSString *email;
@property (nonatomic, retain) NSDate *birthdayDate;
@property (nonatomic, retain) NSURL *avatarURL;
@property (nonatomic, retain) NSString *sex;

@property (nonatomic, retain) DXSEFacebookUserLocation* userLocation;

+ (id) userInfo;

@end
