//
//  DXSEFacebookUserLocation.h
//  MUSocialEngine
//
//  Created by Yuriy Bosov on 12/15/12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DXSEFacebookUserLocation : NSObject

@property (nonatomic, retain) NSString* city;
@property (nonatomic, retain) NSString* country;
@property (nonatomic, retain) NSString* name;
@property (nonatomic, retain) NSString* state;

+ (DXSEFacebookUserLocation*)userLocationWithDictionary:(NSDictionary*)aDictionary;
- (void)setupWithDictionary:(NSDictionary*)aDictionary;

@end
