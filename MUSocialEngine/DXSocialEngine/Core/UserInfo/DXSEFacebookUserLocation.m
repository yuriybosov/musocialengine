//
//  DXSEFacebookUserLocation.m
//  MUSocialEngine
//
//  Created by Yuriy Bosov on 12/15/12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import "DXSEFacebookUserLocation.h"
#import "MUKitDefines.h"

@implementation DXSEFacebookUserLocation

@synthesize city;
@synthesize country;
@synthesize name;
@synthesize state;

+ (DXSEFacebookUserLocation*)userLocationWithDictionary:(NSDictionary*)aDictionary
{
    if (![aDictionary isKindOfClass:[aDictionary class]])
        return nil;
    
    DXSEFacebookUserLocation* ul = [[DXSEFacebookUserLocation new] autorelease];
    [ul setupWithDictionary:aDictionary];
    return ul;
}

- (void)setupWithDictionary:(NSDictionary*)aDictionary
{
    self.city = MU_NULL_PROTECT([aDictionary objectForKey:@"city"]);
    self.country = MU_NULL_PROTECT([aDictionary objectForKey:@"country"]);
    self.name = MU_NULL_PROTECT([aDictionary objectForKey:@"name"]);
    self.state = MU_NULL_PROTECT([aDictionary objectForKey:@"state"]);
}

- (void)dealloc
{
    [city release];
    [country release];
    [name release];
    [state release];
    [super dealloc];
}

- (NSString*)description
{
    return [NSString stringWithFormat:@"city=%@; country=%@; name=%@; state=%@", city, country, name, state];
}

@end
