//
//  MUAppDelegate.m
//  MUSocialEngine
//
//  Created by Yuriy Bosov on 9/11/12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import "MUAppDelegate.h"
#import "DXViewController.h"
#import "DXSESocialEngine.h"


@implementation MUAppDelegate

- (void)dealloc
{
    [_window release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    // Override point for customization after application launch.
    self.viewController = [[[DXViewController alloc] initWithNibName:@"DXViewController" bundle:nil] autorelease];
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [[NSNotificationCenter defaultCenter] postNotificationName:DXSE_REMOVE_FACEBOOK_LOGIN_BLOC object:nil];
}

//SCFacebook Implementation
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    [[NSNotificationCenter defaultCenter] postNotificationName:DXSE_OPEN_URL object:url];
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    [[NSNotificationCenter defaultCenter] postNotificationName:DXSE_OPEN_URL object:url];
    return YES;
}


@end
